var exec = require("cordova/exec");

exports.open = function (options, onSuccess, onError) {
  exec(onSuccess, onError, "CDVBFViewer", "open", [options]);
};