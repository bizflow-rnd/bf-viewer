#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>
#import <Cordova/CDVInvokedUrlCommand.h>
#import <Cordova/CDVUIWebViewDelegate.h>
#import <JavaScriptCore/JavaScriptCore.h>
#import <objc/runtime.h>

#define    TOOLBAR_HEIGHT (44.0 + STATUSBAR_HEIGHT)
#define    STATUSBAR_HEIGHT 20.0
#define    LOCATIONBAR_HEIGHT 21.0
#define    FOOTER_HEIGHT ((TOOLBAR_HEIGHT) + (LOCATIONBAR_HEIGHT))
#define    NAVIGATON_BAR_HEIGHT 44.0
#define    LEFT_MARGIN 60.0

#define    IPHONEX_SAFE_AREA_TOP_PADDING  44.0
#define    IPHONEX_SAFE_AREA_BOTTOM_PADDING 34.0
#define    IPHONEX_OFFSET 20.0
#define    IPHONEX_MARGIN_BOTTOM 10


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

@class BFViewController;

@interface CDVBFViewer : CDVPlugin {
    NSInteger _previousStatusBarStyle;
}
@property (nonatomic, retain) BFViewController* bfViewController;
@property (nonatomic, copy) NSString* callbackId;
- (void)open:(CDVInvokedUrlCommand*)command;
@end



@interface BFViewController :UIViewController<UIWebViewDelegate>{
@private
    NSString* _userAgent;
    NSString* _prevUserAgent;
    CDVUIWebViewDelegate* _webViewDelegate;
    NSInteger _userAgentLockToken;
    NSString* _sessionKey;
    NSString* _closeIcon;
    NSString* _reloadIcon;
    NSString* _backEnableIcon;
    NSString* _backDisableIcon;
    NSString* _forwardEnableIcon;
    NSString* _forwardDisableIcon;
}
@property (nonatomic, strong) IBOutlet UIWebView *webView;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* closeButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* titleButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* forwardButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* backButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* refreshButton;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView* spinner;
@property (nonatomic, strong) IBOutlet UIToolbar* toolbar;
@property (nonatomic, strong) IBOutlet UIToolbar* navigationbar;
@property (nonatomic, strong) IBOutlet UILabel* titleLabel;
@property (nonatomic, strong) IBOutlet UIButton* closeUIButton;
@property (nonatomic, strong) IBOutlet UIButton* refreshUIButton;

@property (nonatomic, weak) CDVBFViewer* navigationDelegate;
@property (nonatomic, copy) NSMutableDictionary *options;
@property (nonatomic, strong) NSTimer *refreshTimer;
@property (nonatomic, assign) float refreshInterval;
@property (nonatomic, copy) NSString *urlOnExit;
@property (nonatomic, assign) bool iPhoneX;

- (void)close;
- (void)navigateToPage;
- (void)showToolBar:(BOOL)show : (NSString *) toolbarPosition;
- (id)initWithUserAgent:(NSString*)userAgent prevUserAgent:(NSString*)prevUserAgent options:(NSMutableDictionary *)options;
- (void)startRefreshTimer;
- (void)stopRefreshTimer;
- (void)refreshPage;

- (IBAction)closeButtonPressed:(id)sender;
- (IBAction)refreshButtonPressed:(id)sender;

@end

@interface BFNavigationController : UINavigationController

@property (nonatomic, weak) id <CDVScreenOrientationDelegate> orientationDelegate;

@end
