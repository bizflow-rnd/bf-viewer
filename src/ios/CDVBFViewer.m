#import "CDVBFViewer.h"

@implementation CDVBFViewer
- (void)pluginInitialize
{
    _previousStatusBarStyle = -1;
}
- (id)settingForKey:(NSString*)key
{
    return [self.commandDelegate.settings objectForKey:[key lowercaseString]];
}

- (void)onReset
{
    [self close:nil];
}

- (void)close:(CDVInvokedUrlCommand*)command
{
    if (self.bfViewController == nil) {
        return;
    }
    // Things are cleaned up in browserExit.
    [self.bfViewController close];
}

- (BOOL) isSystemUrl:(NSURL*)url
{
    if ([[url host] isEqualToString:@"itunes.apple.com"]) {
        return YES;
    }
    
    return NO;
}

- (void)open:(CDVInvokedUrlCommand*)command
{
    NSString* arg = [command argumentAtIndex:0];
    NSMutableDictionary *options = [NSJSONSerialization JSONObjectWithData:[arg dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
    self.callbackId = command.callbackId;

    [self openViewer:options];
}

- (void)openViewer:(NSMutableDictionary *)parameters
{
    //NSLog(@"openViewer");
    if (self.bfViewController == nil) {
        NSString* userAgent = [CDVUserAgentUtil originalUserAgent];
        NSString* overrideUserAgent = [self settingForKey:@"OverrideUserAgent"];
        NSString* appendUserAgent = [self settingForKey:@"AppendUserAgent"];
        if(overrideUserAgent){
            userAgent = overrideUserAgent;
        }
        if(appendUserAgent){
            userAgent = [userAgent stringByAppendingString: appendUserAgent];
        }
        self.bfViewController = [[BFViewController alloc] initWithUserAgent:userAgent prevUserAgent:[self.commandDelegate userAgent] options:parameters];
        self.bfViewController.options = parameters;
        self.bfViewController.navigationDelegate = self;
    }
    
    
    //[self.bfViewController showLocationBar:@"no"];
    [self.bfViewController showToolBar:@"yes" :@"top"];

    
    
    // Set Presentation Style
    UIModalPresentationStyle presentationStyle = UIModalPresentationFullScreen;
    self.bfViewController.modalPresentationStyle = presentationStyle;
    
    // Set Transition Style
    UIModalTransitionStyle transitionStyle = UIModalTransitionStyleCrossDissolve;
    self.bfViewController.modalTransitionStyle = transitionStyle;
    
    if ([self.bfViewController.webView respondsToSelector:@selector(scrollView)]) {
        ((UIScrollView*)[self.bfViewController.webView scrollView]).bounces = NO;
        ((UIScrollView*)[self.bfViewController.webView scrollView]).delaysContentTouches = NO;
        ((UIScrollView*)[self.bfViewController.webView scrollView]).zoomScale = 0.0f;
    } else {
        for (id subview in self.bfViewController.webView.subviews) {
            if ([[subview class] isSubclassOfClass:[UIScrollView class]]) {
                ((UIScrollView*)subview).bounces = NO;
                ((UIScrollView*)subview).delaysContentTouches = NO;
                ((UIScrollView*)subview).zoomScale = 0.0f;
            }
        }
    }
    
    self.bfViewController.webView.userInteractionEnabled = YES;
    self.bfViewController.webView.scalesPageToFit = YES;
    [self.bfViewController navigateToPage];


    IMP wkOriginalImp, uiOriginalImp, nilImp;
    Method wkMethod, uiMethod;
    
    Class wkClass = NSClassFromString([@[@"UI", @"Web", @"Browser", @"View"] componentsJoinedByString:@""]);
    wkMethod = class_getInstanceMethod(wkClass, @selector(inputAccessoryView));
    wkOriginalImp = method_getImplementation(wkMethod);
    Class uiClass = NSClassFromString([@[@"WK", @"Content", @"View"] componentsJoinedByString:@""]);
    uiMethod = class_getInstanceMethod(uiClass, @selector(inputAccessoryView));
    uiOriginalImp = method_getImplementation(uiMethod);
    nilImp = imp_implementationWithBlock(^(id _s) {
        return nil;
    });
    
    method_setImplementation(wkMethod, wkOriginalImp);
    method_setImplementation(uiMethod, uiOriginalImp);

    // method_setImplementation(wkMethod, nilTmp);
    // method_setImplementation(uiMethod, nilTmp);

    
    _previousStatusBarStyle = [UIApplication sharedApplication].statusBarStyle;
    
    __block BFNavigationController* nav = [[BFNavigationController alloc] initWithRootViewController:self.bfViewController];

    nav.navigationBarHidden = YES;
    nav.modalPresentationStyle = self.bfViewController.modalPresentationStyle;
    
    __weak CDVBFViewer* weakSelf = self;
    
    // Run later to avoid the "took a long time" log message.
    dispatch_async(dispatch_get_main_queue(), ^{
        if (weakSelf.bfViewController != nil) {
            CGRect frame = [[UIScreen mainScreen] bounds];
            UIWindow *tmpWindow = [[UIWindow alloc] initWithFrame:frame];
            UIViewController *tmpController = [[UIViewController alloc] init];
            [tmpWindow setRootViewController:tmpController];
            [tmpWindow setWindowLevel:UIWindowLevelNormal];
            
            [tmpWindow makeKeyAndVisible];
            [tmpController presentViewController:nav animated:YES completion:nil];
        }
    });
}

- (void)webViewDidStartLoad:(UIWebView*)theWebView
{
    //NSLog(@"openViewer - webViewDidStartLoad");
}

- (void)webViewDidFinishLoad:(UIWebView*)theWebView
{
    //NSLog(@"openViewer - webViewDidFinishLoad");
}

- (void)webView:(UIWebView*)theWebView didFailLoadWithError:(NSError*)error
{
    //NSLog(@"openViewer - didFailLoadWithError");
}

- (void)browserExit
{
    [self.bfViewController stopRefreshTimer];
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
    
    if (self.callbackId != nil) {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:@{@"message":@"exit"}];                                                   
        [self.commandDelegate sendPluginResult:pluginResult callbackId:self.callbackId];
        self.callbackId = nil;
    }

    self.bfViewController.navigationDelegate = nil;
    self.bfViewController = nil;
    
    if (IsAtLeastiOSVersion(@"7.0")) {
        if (_previousStatusBarStyle != -1) {
            [[UIApplication sharedApplication] setStatusBarStyle:_previousStatusBarStyle];
        }
    }
    
    _previousStatusBarStyle = -1; // this value was reset before reapplying it. caused statusbar to stay black on ios7
}

@end


@implementation BFViewController

-(void) viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
  
  if(self.iPhoneX){
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinator> context){
      UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
      CGRect toolbarFrame;
      CGFloat titleBarY = 0;
      UIBarButtonItem* fixedSpaceButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
      fixedSpaceButton.width = 30;
      UIBarButtonItem *leftMargin;
      
      if(orientation == UIDeviceOrientationPortrait){
        [self.webView setFrame:CGRectMake(0, IPHONEX_SAFE_AREA_TOP_PADDING*2, self.view.bounds.size.width, self.view.bounds.size.height-IPHONEX_SAFE_AREA_BOTTOM_PADDING-IPHONEX_SAFE_AREA_TOP_PADDING*3)];

          toolbarFrame = CGRectMake(0.0, IPHONEX_SAFE_AREA_TOP_PADDING, self.view.bounds.size.width, TOOLBAR_HEIGHT-STATUSBAR_HEIGHT);
          titleBarY = 0;
        [self.toolbar setFrame:toolbarFrame];
        CGRect navToolBarFrame = CGRectMake(0.0, self.view.bounds.size.height - NAVIGATON_BAR_HEIGHT-IPHONEX_SAFE_AREA_BOTTOM_PADDING, self.view.bounds.size.width, NAVIGATON_BAR_HEIGHT);
        [self.navigationbar setFrame:navToolBarFrame];
        
        leftMargin = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:NULL];
        leftMargin.width = -12;

        [self.navigationbar setItems:@[leftMargin,self.backButton,fixedSpaceButton,self.forwardButton]];
      } else if(orientation == UIDeviceOrientationLandscapeRight){
 
      } else  if(orientation == UIDeviceOrientationLandscapeLeft){
        [self.webView setFrame:CGRectMake(0, IPHONEX_SAFE_AREA_TOP_PADDING + IPHONEX_OFFSET, self.view.bounds.size.width, self.view.bounds.size.height-IPHONEX_SAFE_AREA_BOTTOM_PADDING-IPHONEX_SAFE_AREA_TOP_PADDING - IPHONEX_OFFSET*2 + IPHONEX_MARGIN_BOTTOM)];
        
          toolbarFrame = CGRectMake(0.0, IPHONEX_OFFSET, self.view.bounds.size.width, TOOLBAR_HEIGHT-STATUSBAR_HEIGHT);
          titleBarY = 0;
        [self.toolbar setFrame:toolbarFrame];
        CGRect navToolBarFrame = CGRectMake(0.0, self.view.bounds.size.height - NAVIGATON_BAR_HEIGHT, self.view.bounds.size.width, NAVIGATON_BAR_HEIGHT);
        [self.navigationbar setFrame:navToolBarFrame];
        
        leftMargin = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:NULL];
        leftMargin.width = -12*8;

        [self.navigationbar setItems:@[leftMargin,self.backButton,fixedSpaceButton,self.forwardButton]];
        
      }
    } completion:^(id<UIViewControllerTransitionCoordinatorContext>context){
      
    }];
  }
  
  [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}



- (void)showToolBar:(BOOL)show : (NSString *) toolbarPosition
{
    CGRect toolbarFrame = self.toolbar.frame;
    CGRect navbarFrame = self.navigationbar.frame;

    self.toolbar.hidden = NO;
    CGRect webViewBounds = self.view.bounds;

    webViewBounds.size.height -= (TOOLBAR_HEIGHT + NAVIGATON_BAR_HEIGHT);
    self.toolbar.frame = toolbarFrame;


    toolbarFrame.origin.y = 0;
    navbarFrame.origin.y = self.view.bounds.size.height - NAVIGATON_BAR_HEIGHT;
    
    webViewBounds.origin.y += toolbarFrame.size.height;
    [self setWebViewFrame:webViewBounds];

}

- (id)initWithUserAgent:(NSString*)userAgent prevUserAgent:(NSString*)prevUserAgent options:(NSMutableDictionary *)options
{
    self = [super init];
    if (self != nil) {
        _userAgent = userAgent;
        _prevUserAgent = prevUserAgent;
        _closeIcon = [options objectForKey:@"closeIcon"];
        _reloadIcon = [options objectForKey:@"reloadIcon"];
        _backEnableIcon = [options objectForKey:@"backEnableIcon"];
        _backDisableIcon = [options objectForKey:@"backDisableIcon"];
        _forwardEnableIcon = [options objectForKey:@"forwardEnableIcon"];
        _forwardDisableIcon = [options objectForKey:@"forwardDisableIcon"];
        _webViewDelegate = [[CDVUIWebViewDelegate alloc] initWithDelegate:self];
        [self createViews];
    }
    
    return self;
}

// Prevent crashes on closing windows
-(void)dealloc {
    self.webView.delegate = nil;
}

- (void)createViews
{
  
    //NSLog(@"createViews");
    CGRect webViewBounds = self.view.bounds;

  
    BOOL toolbarIsAtBottom = false;
    //webViewBounds.size.height -= (FOOTER_HEIGHT + NAVIGATON_BAR_HEIGHT);
    self.webView = [[UIWebView alloc] initWithFrame:webViewBounds];
  
    self.webView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
//    self.webView.layer.borderWidth =2;
//    self.webView.layer.borderColor = [UIColor redColor].CGColor;
  
//  NSString *modelId = [[UIDevice currentDevice] model];
//  self.iPhoneX = [modelId isEqualToString:@"iPhone10,3"] || [modelId isEqualToString:@"iPhone10,6"];
//  NSLog(@"model:%@",modelId);
    if(@available(iOS 11.0, *)){
      UIWindow *window = UIApplication.sharedApplication.keyWindow;
      CGFloat topPadding = window.safeAreaInsets.top;
      if(topPadding > 0){
        self.iPhoneX = YES;
      } else {
        self.iPhoneX = NO;
      }
    }
  
    [self.view addSubview:self.webView];
    [self.view sendSubviewToBack:self.webView];

    
    self.webView.delegate = _webViewDelegate;
    self.webView.backgroundColor = [UIColor whiteColor];
    
    self.webView.clearsContextBeforeDrawing = YES;
    self.webView.clipsToBounds = YES;
    self.webView.contentMode = UIViewContentModeScaleAspectFit;
    self.webView.multipleTouchEnabled = YES;
    self.webView.opaque = YES;
    self.webView.scalesPageToFit = YES;
    self.webView.userInteractionEnabled = YES;
    
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner.alpha = 1.000;
    self.spinner.autoresizesSubviews = YES;
    self.spinner.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
                                     UIViewAutoresizingFlexibleTopMargin |
                                     UIViewAutoresizingFlexibleBottomMargin |
                                     UIViewAutoresizingFlexibleRightMargin);
    self.spinner.clearsContextBeforeDrawing = NO;
    self.spinner.clipsToBounds = NO;
    self.spinner.contentMode = UIViewContentModeScaleToFill;
    self.spinner.frame = CGRectMake(CGRectGetMidX(self.webView.frame), CGRectGetMidY(self.webView.frame), 20.0, 20.0);
    self.spinner.hidden = NO;
    self.spinner.hidesWhenStopped = YES;
    self.spinner.multipleTouchEnabled = NO;
    self.spinner.opaque = NO;
    self.spinner.userInteractionEnabled = NO;
    [self.spinner stopAnimating];
  
    CGRect toolbarFrame;
    CGRect navToolBarFrame;
    CGFloat titleBarY = 0;
    if(self.iPhoneX == YES){
      toolbarFrame = CGRectMake(0.0, IPHONEX_SAFE_AREA_TOP_PADDING, self.view.bounds.size.width, TOOLBAR_HEIGHT-STATUSBAR_HEIGHT);
      titleBarY = 0;
      navToolBarFrame = CGRectMake(0.0, self.view.bounds.size.height - NAVIGATON_BAR_HEIGHT-IPHONEX_SAFE_AREA_BOTTOM_PADDING, self.view.bounds.size.width, NAVIGATON_BAR_HEIGHT);
    } else {
      toolbarFrame = CGRectMake(0.0, 0.0, self.view.bounds.size.width, TOOLBAR_HEIGHT);
      titleBarY = LOCATIONBAR_HEIGHT;
      navToolBarFrame = CGRectMake(0.0, self.view.bounds.size.height - NAVIGATON_BAR_HEIGHT, self.view.bounds.size.width, NAVIGATON_BAR_HEIGHT);
    }
  
    self.toolbar = [[UIToolbar alloc] initWithFrame:toolbarFrame];
    self.toolbar.alpha = 1.000;
    self.toolbar.autoresizesSubviews = YES;
    self.toolbar.barStyle = UIBarStyleBlackTranslucent;
    self.toolbar.clearsContextBeforeDrawing = NO;
    self.toolbar.clipsToBounds = NO;
    self.toolbar.contentMode = UIViewContentModeScaleToFill;
    self.toolbar.autoresizingMask = toolbarIsAtBottom ? (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin) : UIViewAutoresizingFlexibleWidth;
    self.toolbar.hidden = NO;
    self.toolbar.multipleTouchEnabled = NO;
    self.toolbar.opaque = YES;
    self.toolbar.userInteractionEnabled = YES;
    self.toolbar.barTintColor = [UIColor colorWithRed:0.07 green:0.26 blue:0.45 alpha:1.0];
    
    
    self.navigationbar = [[UIToolbar alloc] initWithFrame:navToolBarFrame];
    self.navigationbar.alpha = 1.000;
    self.navigationbar.autoresizesSubviews = YES;
    self.navigationbar.barStyle = UIBarStyleDefault;
    self.navigationbar.clearsContextBeforeDrawing = NO;
    self.navigationbar.clipsToBounds = NO;
    self.navigationbar.contentMode = UIViewContentModeScaleToFill;
    self.navigationbar.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin);
    self.navigationbar.hidden = NO;
    self.navigationbar.multipleTouchEnabled = NO;
    self.navigationbar.opaque = YES;
    self.navigationbar.userInteractionEnabled = YES;
    self.navigationbar.barTintColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1.0];
    
    UIBarButtonItem *leftMargin = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:NULL];
    leftMargin.width = 3;
    UIBarButtonItem *rightMargin = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:NULL];
    rightMargin.width = -12;

    UIBarButtonItem* fixedSpaceButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpaceButton.width = 30;
    
    
    // toolbar button
    self.closeUIButton = [[UIButton alloc] initWithFrame:CGRectMake(0, titleBarY, LEFT_MARGIN, NAVIGATON_BAR_HEIGHT)];
//    [self.closeUIButton setTitle:@"Close" forState:UIControlStateNormal];
    [self.closeUIButton setImage:[self getIcon:_closeIcon iconDensity:2] forState:UIControlStateNormal];
    self.closeUIButton.userInteractionEnabled = true;
    [self.closeUIButton addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN,titleBarY, self.webView.bounds.size.width - LEFT_MARGIN*2, NAVIGATON_BAR_HEIGHT)];
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor whiteColor];
    
    self.titleLabel.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin);
    
    self.refreshUIButton = [[UIButton alloc] initWithFrame:CGRectMake(self.webView.bounds.size.width - LEFT_MARGIN, titleBarY, LEFT_MARGIN, NAVIGATON_BAR_HEIGHT)];
    [self.refreshUIButton setImage:[self getIcon:_reloadIcon iconDensity:2] forState:UIControlStateNormal];
    self.refreshUIButton.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin);
    [self.refreshUIButton addTarget:self action:@selector(refreshPage) forControlEvents:UIControlEventTouchUpInside];
    
    [self.toolbar layoutIfNeeded];//known bug on iOS 11, UIToolbar subviews don't get the touch events because some internal views to the toolbar aren't properly setup
    [self.toolbar addSubview:self.closeUIButton];
    [self.toolbar addSubview:self.titleLabel];
    [self.toolbar addSubview:self.refreshUIButton];
    
    // navigationbar button
    self.forwardButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(goForward:)];
    self.forwardButton.enabled = YES;
    [self.forwardButton setBackgroundImage:[self getIcon:_forwardEnableIcon iconDensity:2] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self.forwardButton setBackgroundImage:[self getIcon:_forwardDisableIcon iconDensity:2] forState:UIControlStateDisabled barMetrics:UIBarMetricsDefault];
    
    self.backButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(goBack:)];
    self.backButton.enabled = YES;
    [self.backButton setBackgroundImage:[self getIcon:_backEnableIcon iconDensity:2] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self.backButton setBackgroundImage:[self getIcon:_backDisableIcon iconDensity:2] forState:UIControlStateDisabled barMetrics:UIBarMetricsDefault];

//// Font Button
//    NSString* frontArrowString = @">";
//    self.forwardButton = [[UIBarButtonItem alloc] initWithTitle:frontArrowString style:UIBarButtonItemStylePlain target:self action:@selector(goForward:)];
//    self.forwardButton.enabled = YES;
//    [self.forwardButton setTitleTextAttributes:@{
//                                               NSForegroundColorAttributeName:[UIColor colorWithRed:0.11 green:0.45 blue:0.76 alpha:1.0],
//                                               NSFontAttributeName: [UIFont systemFontOfSize:26]
//                                               } forState:UIControlStateNormal];
//    [self.forwardButton setTitleTextAttributes:@{
//                                              NSForegroundColorAttributeName:[UIColor colorWithRed:0.85 green:0.85 blue:0.85 alpha:1.0],
//                                              NSFontAttributeName: [UIFont systemFontOfSize:26]
//                                              } forState:UIControlStateDisabled];

    [self.navigationbar setItems:@[leftMargin,self.backButton,fixedSpaceButton,self.forwardButton]];
    

    [self.view addSubview:self.toolbar];
    [self.view addSubview:self.navigationbar];
    [self.view addSubview:self.spinner];
    
    [self.webView setDataDetectorTypes: UIDataDetectorTypeNone];
    

}

- (void) refreshToolBar{

  
}

- (UIImage*) getIcon:(NSString*) iconPath iconDensity:(CGFloat) iconDensity
{
    UIImage *icon;
    if (iconPath) {
        NSString* path = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:[NSString pathWithComponents:@[@"www", iconPath]]];
        if (!iconDensity) {
            iconDensity = 1.0;
        }
        NSData* data = [NSData dataWithContentsOfFile:path];
        icon = [UIImage imageWithData:data scale:iconDensity];
        icon = [icon imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    return icon;
}

- (void) setWebViewFrame : (CGRect) frame {
    //NSLog(@"Setting the WebView's frame to %@", NSStringFromCGRect(frame));
    [self.webView setFrame:frame];
}


- (void)viewDidLoad
{

    //NSLog(@"viewDidLoad");
    [self.spinner stopAnimating];
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (void)close
{
    //NSLog(@"close");
    [CDVUserAgentUtil releaseLock:&_userAgentLockToken];
    
    if ((self.navigationDelegate != nil) && [self.navigationDelegate respondsToSelector:@selector(browserExit)]) {
        [self.navigationDelegate browserExit];
    }
    
    __weak UIViewController* weakSelf = self;
    
    // Run later to avoid the "took a long time" log message.
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([weakSelf respondsToSelector:@selector(presentingViewController)]) {
            [[weakSelf presentingViewController] dismissViewControllerAnimated:YES completion:nil];
        } else {
            [[weakSelf parentViewController] dismissViewControllerAnimated:YES completion:nil];
        }
    });
}

- (void)navigateToPage{
//    for(NSString *key in [self.options allKeys]){
//        NSLog(@"value:%@, key:%@",[self.options objectForKey:key ], key);
//    }
    
    NSString *reqMethod = [[self.options objectForKey:@"datasource"] objectForKey:@"method"];
    self.urlOnExit = [[self.options objectForKey:@"datasource"] objectForKey:@"urlOnExit"];
    NSString *targetServer = [NSString stringWithFormat:@"http://%@", [self.options objectForKey:@"targetServer"]];//target server
    NSString *title = [self.options objectForKey:@"name"];
    self.titleLabel.text = title;
    NSString *targetUrl = [self.options objectForKey:@"targetUrl"];//datasource url
    NSString *sessionKey = [self.options objectForKey:@"sessionKey"];
    //NSString *sessionCookie = [self.options objectForKey:@"session"];
    NSURL *_url;
    NSMutableURLRequest *request;
    
    
    
    float _interval = [[self.options objectForKey:@"refreshInterval"] floatValue];//minutes
    self.refreshInterval = _interval * 60;
    [self startRefreshTimer];
    
    NSString *requestData = @"";
    NSString *requestParam = @"";
    NSString *requestURL = @"";
    
    NSRange range = [targetUrl rangeOfString:@"?"];
    BOOL hasQueryParam = false;
    if(range.location != NSNotFound){
        hasQueryParam = true;
    } else {
        hasQueryParam = false;
    }
    
    NSUInteger index = 0;
    NSDictionary *parameters = [[self.options objectForKey:@"datasource"] objectForKey:@"parameter"];
    
    if([[reqMethod uppercaseString] isEqualToString:@"POST"]){
        //NSLog(@"request method:POST");
        if(parameters != nil){
            for (NSDictionary *dict in parameters) {
                NSString *name = [dict objectForKey:@"name"];
                NSString *value = [dict objectForKey:@"value"];
                requestData = [NSString stringWithFormat:@"&%@=%@%@", name, value,requestData];
            }
        }
        
        requestURL = [targetServer stringByAppendingString:targetUrl];
        _url = [NSURL URLWithString:requestURL];
        request = [NSMutableURLRequest requestWithURL:_url];
        
        [request setHTTPMethod:reqMethod];
        NSString *length = [NSString stringWithFormat:@"%lu",(unsigned long)[requestData length]];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setValue:length forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:[requestData dataUsingEncoding:NSASCIIStringEncoding]];
    } else {
        //NSLog(@"request method:GET or NONE");
        if(parameters != nil){
            index = 0;
            for (NSDictionary *dict in parameters) {
                NSString *name = [dict objectForKey:@"name"];
                NSString *value = [dict objectForKey:@"value"];
                if(hasQueryParam){
                    NSString *param = [NSString stringWithFormat:@"&%@=%@", name, value];
                    requestData = [requestData stringByAppendingString:param];
                } else {
                    if(index == 0){
                        NSString *param = [NSString stringWithFormat:@"?%@=%@", name, value];
                        requestData = [requestData stringByAppendingString:param];
                    } else {
                        NSString *param = [NSString stringWithFormat:@"&%@=%@", name, value];
                        requestData = [requestData stringByAppendingString:param];
                    }
                }
                index++;
            }
        }
        requestParam = [targetUrl stringByAppendingString:requestData];
        requestURL = [targetServer stringByAppendingString:requestParam];
    
        _url = [NSURL URLWithString:[requestURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
        
        request = [NSMutableURLRequest requestWithURL:_url];

        [request setHTTPMethod:@"GET"];
        [request setValue:@"text/html;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
    }

    [request setValue:sessionKey forHTTPHeaderField:@"x-bizflow-session-key"];
    //NSLog(@"req data:%@",request);
    [self.webView loadRequest:request];
}

- (void)goBack:(id)sender
{
    [self.webView goBack];

}

- (IBAction)closeButtonPressed:(id)sender
{
    //NSLog(@"closeButtonPressed");
    [self close];
}

- (IBAction)refreshButtonPressed:(id)sender
{
    //NSLog(@"refreshButtonPressed");
    [self.webView reload];
}

- (void)goForward:(id)sender
{
    [self.webView goForward];
}

- (void)startRefreshTimer{
    //NSLog(@"startRefreshTimer:%f",self.refreshInterval);
    if(self.refreshInterval != 0.0){
        self.refreshTimer = [NSTimer scheduledTimerWithTimeInterval:self.refreshInterval target:self selector:@selector(refreshPage) userInfo:nil repeats:YES];
    } else {
        self.refreshTimer = nil;
    }
}

- (void)refreshPage{
    //NSLog(@"refreshPage");
    [self.webView reload];
}


- (void)stopRefreshTimer{
    if(self.refreshTimer != nil){
        [self.refreshTimer invalidate];
        self.refreshTimer = nil;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    if (IsAtLeastiOSVersion(@"7.0")) {
        [[UIApplication sharedApplication] setStatusBarStyle:[self preferredStatusBarStyle]];
    }
    [self refreshLayout];
    
    [super viewWillAppear:animated];
}

//
// On iOS 7 the status bar is part of the view's dimensions, therefore it's height has to be taken into account.
// The height of it could be hardcoded as 20 pixels, but that would assume that the upcoming releases of iOS won't
// change that value.
//
- (float) getStatusBarOffset {
    CGRect statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
    float statusBarOffset = IsAtLeastiOSVersion(@"7.0") ? MIN(statusBarFrame.size.width, statusBarFrame.size.height) : 0.0;
    return statusBarOffset;
}

- (void) refreshLayout {
  NSLog(@"refreshLayout");
  if(self.iPhoneX){
    [self.webView setFrame:CGRectMake(0, IPHONEX_SAFE_AREA_TOP_PADDING*2, self.view.bounds.size.width, self.view.bounds.size.height-IPHONEX_SAFE_AREA_BOTTOM_PADDING-IPHONEX_SAFE_AREA_TOP_PADDING*3)];
  }
}

- (void) resetNavigateButtonEnable:(UIWebView*)theWebView {
    self.backButton.enabled = theWebView.canGoBack;
    self.forwardButton.enabled = theWebView.canGoForward;
}

#pragma mark UIWebViewDelegate



- (void)webViewDidStartLoad:(UIWebView*)theWebView
{
    //NSLog(@"UIWebViewDelegate - webViewDidStartLoad");
    //NSLog(@"webViewDidStartLoad");
    // loading url, start spinner, update back/forward
    [self resetNavigateButtonEnable:theWebView];
    
    self.backButton.enabled = theWebView.canGoBack;
    self.forwardButton.enabled = theWebView.canGoForward;
    
    [self.spinner startAnimating];
    return [self.navigationDelegate webViewDidStartLoad:theWebView];
}

- (BOOL)webView:(UIWebView*)theWebView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
    //NSLog(@"UIWebViewDelegate - shouldStartLoadWithRequest");
    [self resetNavigateButtonEnable:theWebView];
    
    NSURL *url = request.URL;
  
    [self.spinner stopAnimating];
    if([[url scheme] isEqualToString:@"about"]){
        return NO;
    }
//
//    NSLog(@"UIWebViewDelegate - shouldStartLoadWithRequest - url: %@",url.absoluteString);
//    NSLog(@"UIWebViewDelegate - shouldStartLoadWithRequest - url scheme: %@",[url scheme]);
//    NSLog(@"UIWebViewDelegate - shouldStartLoadWithRequest - navigationType: %ld",(long)navigationType);
//
//    switch(navigationType){
//      case UIWebViewNavigationTypeLinkClicked:
//        NSLog(@"===UIWebViewNavigationTypeLinkClicked===");
//        break;
//      case UIWebViewNavigationTypeFormSubmitted:
//        NSLog(@"===UIWebViewNavigationTypeFormSubmitted===");
//        break;
//      case UIWebViewNavigationTypeBackForward:
//        NSLog(@"===UIWebViewNavigationTypeBackForward===");
//        break;
//      case UIWebViewNavigationTypeReload:
//        NSLog(@"===UIWebViewNavigationTypeReload===");
//        break;
//      case UIWebViewNavigationTypeFormResubmitted:
//        NSLog(@"===UIWebViewNavigationTypeFormResubmitted===");
//        break;
//      case UIWebViewNavigationTypeOther:
//        NSLog(@"===UIWebViewNavigationTypeOther===");
//        break;
//
//      default:
//        NSLog(@"===navigationType - default===");
//        break;
//    }
  
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView*)theWebView
{
    //NSLog(@"webViewDidFinishLoad");
    //NSLog(@"UIWebViewDelegate - webViewDidFinishLoad");
    [self resetNavigateButtonEnable:theWebView];
    
    theWebView.scalesPageToFit = YES;
    theWebView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.spinner stopAnimating];
    [self.navigationDelegate webViewDidFinishLoad:theWebView];
    
}
- (void)webView:(UIWebView*)theWebView didFailLoadWithError:(NSError*)error
{
    //NSLog(@"didFailLoadWithError");
    // log fail message, stop spinner, update back/forward
    [self resetNavigateButtonEnable:theWebView];
    
    //NSLog(@"UIWebViewDelegate - didFailLoadWithError");
    [self.spinner stopAnimating];

}

@end




@implementation BFNavigationController : UINavigationController

- (void) dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion {
    if ( self.presentedViewController) {
        [super dismissViewControllerAnimated:flag completion:completion];
    }
}

- (void) viewDidLoad {
    
    CGRect statusBarFrame = [self invertFrameIfNeeded:[UIApplication sharedApplication].statusBarFrame];
    statusBarFrame.size.height = STATUSBAR_HEIGHT;

    
    UIToolbar* bgToolbar = [[UIToolbar alloc] initWithFrame:statusBarFrame];
    bgToolbar.barStyle = UIBarStyleDefault;
    [bgToolbar setAutoresizingMask:UIViewAutoresizingFlexibleWidth];

    [self.view addSubview:bgToolbar];
    
    [super viewDidLoad];
}

- (CGRect) invertFrameIfNeeded:(CGRect)rect {
    // We need to invert since on iOS 7 frames are always in Portrait context
    if (!IsAtLeastiOSVersion(@"8.0")) {
        if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
            CGFloat temp = rect.size.width;
            rect.size.width = rect.size.height;
            rect.size.height = temp;
        }
        rect.origin = CGPointZero;
    }
    return rect;
}

- (BOOL)shouldAutorotate
{
    if ((self.orientationDelegate != nil) && [self.orientationDelegate respondsToSelector:@selector(shouldAutorotate)]) {
        return [self.orientationDelegate shouldAutorotate];
    }
    return YES;
}

//- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations
//{
//    if ((self.orientationDelegate != nil) && [self.orientationDelegate respondsToSelector:@selector(supportedInterfaceOrientations)]) {
//        return [self.orientationDelegate supportedInterfaceOrientations];
//    }
//    return 1 << UIInterfaceOrientationLandscapeRight;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ((self.orientationDelegate != nil) && [self.orientationDelegate respondsToSelector:@selector(shouldAutorotateToInterfaceOrientation:)]) {
        return [self.orientationDelegate shouldAutorotateToInterfaceOrientation:interfaceOrientation];
    }
    
    return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
}

@end

